%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function []=main()
disp('codes for paper to stabilisation');figure(1);
sw=4;
if (sw==1)
    example_lyapunov(100);
elseif (sw==2)
    table(); 
elseif (sw==3)
    eos();
elseif (sw==4)
    lxfrun();
end

disp('END');
return;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function []=lxfrun()
% implements enquist osher for nonlinear conservation law
Nx=[25,50,100,200]; 
for i=1:length(Nx)
    par=setparameters(Nx(i));
    par.burgers=1; %other option  =0 is supply chain example
    % all initial data is bounded by rho0.

    % recompute dt.
    par.dt = par.dx ./ 2 ./ par.rho0; 
    if (par.burgers==0), par.dt = par.dx; end; 
    par.nu = par.rho0; %Burgers
    if (par.burgers==0), par.nu = 1./(1+2.*par.rho0); end;
    par.mu = par.nu * ( exp(1) - 1 ) ./ exp(1); 

    x0  = par.rho0 .* sin(2.*pi.*par.xc); 
    L0  = eoslyap(x0,par);
    xT  = lxfforward(x0,par);
    LT  = eoslyap(xT,par);
    Lbd = exp( - par.mu  .* par.T ).*L0;

    fprintf('\n %d Initial: %f  Terminal: %f  Theory: %f Delta t: %f',Nx(i),L0,LT,Lbd,par.dt);
    
end
fprintf('\n T=%f kappa=%f',par.T,par.kappa);

Nx=100;clf; hold on; xlabel('t'); 
T=linspace(1,20,60);
for i=1:length(T)
   par=setparameters(Nx);
   par.T=T(i); 
   par.burgers=0; %other option  =0 is supply chain example
   par.dt = par.dx ./ 2 ./ par.rho0; 
   if (par.burgers==0), par.dt = par.dx; end; 
   par.nu = par.rho0; %Burgers
   if (par.burgers==0), par.nu = 1./(1+2.*par.rho0); end;
   par.mu = par.nu * ( exp(1) - 1 ) ./ exp(1); 
   x0  = par.rho0 .* sin(2.*pi.*par.xc); 
   xT  = lxfforward(x0,par);
   LT  = eoslyap(xT,par);
   Lbd = exp( - par.mu  .* par.T ).*L0;
   plot(T(i),log(LT)./log(10),'bx',T(i),log(Lbd)./log(10),'ro',T(i),log(par.dt)./log(10),'gs'); drawnow;
end
L0  = eoslyap(x0,par);
plot(0,L0,'bx');
print('lxfnlburgeru4.png','-dpng');
save;keyboard;
return;
function x=lxfforward(x0,par)
t=0;x=x0;
while(t<par.T)
        x = lxfsingle(x,par.kappa.*x(par.N), par.kappa.*x(1),par);
        if (t+par.dt>=par.T), par.dt=par.T-t; end;
        t=t+par.dt;
end
return;
function y=lxfsingle(x,bl,br,par)
% computes one step of the enquist-osher scheme in the nonlinear case
% with left boundary condition bl and right condition br
lambda=par.dt./par.dx;
df  = eosflux([x(2:par.N),br],par)-eosflux([bl,x(1:par.N-1)],par);
y(1:par.N) = 1./2 .*( [x(2:par.N),br] + [bl,x(1:par.N-1)] ) - lambda./2.*df;
return
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function []=eos()
% implements enquist osher for nonlinear conservation law
Nx=[25,50,100,200]; 
for i=1:length(Nx)
    par=setparameters(Nx(i));
    par.burgers=1; %other option  =0 is supply chain example
    % all initial data is bounded by rho0.

    % recompute dt.
    par.dt = par.dx ./ 2 ./ par.rho0; 
    if (par.burgers==0), par.dt = par.dx; end; 
    par.nu = par.rho0; %Burgers
    if (par.burgers==0), par.nu = 1./(1+2.*par.rho0); end;
    par.mu = par.nu * ( exp(1) - 1 ) ./ exp(1); 

    x0  = par.rho0 .* sin(2.*pi.*par.xc); 
    L0  = eoslyap(x0,par);
    xT  = eosforward(x0,par);
    LT  = eoslyap(xT,par);
    Lbd = exp( - par.mu  .* par.T ).*L0;

    fprintf('\n %d Initial: %f  Terminal: %f  Theory: %f Delta t: %f',Nx(i),L0,LT,Lbd,par.dt);
end
fprintf('\n T=%f kappa=%f',par.T,par.kappa);

Nx=100;clf; hold on; xlabel('t'); 
T=linspace(1,20,60);
for i=1:length(T)
   par=setparameters(Nx);
   par.T=T(i); 
   par.burgers=1; %other option  =0 is supply chain example
   par.dt = par.dx ./ 2 ./ par.rho0; 
   if (par.burgers==0), par.dt = par.dx; end; 
   par.nu = par.rho0; %Burgers
   if (par.burgers==0), par.nu = 1./(1+2.*par.rho0); end;
   par.mu = par.nu * ( exp(1) - 1 ) ./ exp(1); 
   x0  = par.rho0 .* sin(2.*pi.*par.xc); 
   xT  = eosforward(x0,par);
   LT  = eoslyap(xT,par);
   Lbd = exp( - par.mu  .* par.T ).*L0;
   plot(T(i),log(LT)./log(10),'bx',T(i),log(Lbd)./log(10),'ro',T(i),log(par.dt)./log(10),'gs'); drawnow;
end
L0  = eoslyap(x0,par);
plot(0,L0,'bx');
print('EOnlscmu4.png','-dpng');
save;keyboard;
return;
function [y]=eosflux(u,par)
% nonlinear flux of linearized system 
% Burgers
y = 0.5 .* (u + par.rho0).^2;
% Supply chain
if (par.burgers==0), y= (u+par.rho0)./(1+u+par.rho0); end;
return;
function L=eoslyap(x,par)
L=par.dx.*sum(  exp(-par.xc).*eoseta(x) );
return
function y=eoseta(x,par)
% eta: only requirment is eta'\geq0 for x\geq0 and eta'\leq 0 for x \leq 0
% and eta(0)=0
y=x.*x.*x.*x; 
return
function x=eosforward(x0,par)
t=0;x=x0;
while(t<par.T)
        x = eossingle(x,par.kappa.*x(par.N),par);
        if (t+par.dt>=par.T), par.dt=par.T-t; end;
        t=t+par.dt;
end
return;
function y=eossingle(x,bl,par)
% computes one step of the enquist-osher scheme in the nonlinear case
% with left boundary condition bl.
y(1:par.N)=x(1:par.N)-par.dt./par.dx.*(eosflux(x(1:par.N),par) - eosflux([bl,x(1:par.N-1)],par));
return
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function []=table()
% compare experimental rate and numerical rate
Nx=[25,50,100,200]; 
fprintf('\n Nx  mu(exp)  mu(theo) \n');
for i=1:length(Nx)
   par = setparameters(Nx(i)); lambda=par.dt/par.dx;
   x0  = 4.*sin(4.*pi.*par.xc); 
   L0  = lyap(x0,par);
   xT  = lxft(x0,par);
   LT  = lyap(xT,par);
   tp  = -log(LT/L0)/par.T;
   tth = par.mu/2/lambda;
   fprintf(' %d  & %5.2f & %5.2f  \n',Nx(i),tp,tth);
end
fprintf('\n data T=%f',par.T);
return; 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function []=example_lyapunov(N)
par=setparameters(N);
lambda=par.dt/par.dx;
fprintf('\n T=%f dx=%f a*lambda =%f D=%f',par.T,par.dx, lambda.*par.a, 1/2*(1+par.a*lambda));
t=0;k=1;
x=sin(2.*pi.*par.xc); L(k) = lyap(x,par); tc(1)=t;
while(t<par.T)
    x = lxf(x,par.kappa.*x(par.N),par.kappa.*x(1),par);
    k=k+1;
    if (t+par.dt>=par.T), par.dt=par.T-t; end;
    t=t+par.dt;
    tc(k)=t;
    L(k) = lyap(x,par); 
 %   clf; plot(par.xc,x,'r'); 
 %   title(sprintf('t=%f L(k)=%f L(k)-L(k-1)=%f',t,L(k),(L(k)-L(k-1))./par.dt)); drawnow; 
end
clf; plot(tc,log(L)./log(10),'bx',tc,log(exp(-par.mu/lambda.*(tc)).*L(1))./log(10),'rx');
xlabel('t'); 
print('kappbcaneg.png','-dpng');
save; keyboard; 
return;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function par=setparameters(N)
par.N=N; % gridpoints
par.dx=1./(N+1);
par.T=2;
par.xc=linspace(par.dx,1-par.dx,N);
par.cfl=exp(1)/(exp(1)+1); % cfl constant
par.a=-1; % maximal eigenvalue
par.dt = par.cfl .* par.dx ./ abs(par.a);
% lyapundov function
par.mu= (exp(1)-1) ./ exp(1)./2;
par.kappa = sqrt( 1./exp(1) );
% steady state (for EO-Computations)
par.rho0 = 1;
return;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function x=lxft(x0,par)
t=0;x=x0;
while(t<par.T)
        x = lxf(x,par.kappa.*x(par.N),par.kappa.*x(1),par);
        if (t+par.dt>=par.T), par.dt=par.T-t; end;
        t=t+par.dt;
end
return;
function y=lxf(x,bl,br,par)
% computes one step of the lax-friedrich in the linear case
d = 1./2.*( 1 + par.a .* par.dt ./ par.dx );
y(1:par.N) = [x(2:par.N),br] .* (1-d) + d.* [bl,x(1:par.N-1)];
return
function L=lyap(x,par)
L=par.dx.*sum(  exp(-sign(par.a).*par.xc).*x.*x );
return
